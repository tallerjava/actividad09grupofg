import java.io.IOException;
import org.junit.Test;

public class UsuarioServiceTest {
    
    public UsuarioServiceTest() {
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsername_valorNull_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarUsername(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsername_StringVacio_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarUsername("");
    } 
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsername_MenorAlRango_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarUsername("asd");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsername_MayorAlRango_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarUsername("asdrewyno");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsername_DentroDelRangoConCaracterPipe_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarUsername("asd|we");
    }
    
    @Test
    public void validarUsername_DentroDelRangoSinCaracterPipe_UsernameValido() throws IllegalArgumentException{
        UsuarioService.validarUsername("asdewe");
    }
    
        @Test(expected = IllegalArgumentException.class)
    public void validarPassword_valorNull_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_StringVacio_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("");
    } 
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_MenorAlRango_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("asdasda");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_MayorAlRango_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("asd321dasd48689dqwd13");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_DentroDelRangoConCaracterPipe_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("asd|weasdasf");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_DentroDelRangoSinDigito_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("asdewasdae");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_DentroDelRangoConCaracterPipeConDigito_IllegalArgumentException() throws IllegalArgumentException{
        UsuarioService.validarPassword("fsfasd|wf12ase");
    }
    
    @Test
    public void validarPassword_DentroDelRangoSinCaracterPipeConDigito_PasswordValido() throws IllegalArgumentException{
        UsuarioService.validarPassword("asdeasg26we");
    }
    
    /*@Test
    public void guardar_RegistroUsuario_registroValido() throws IOException{
        Usuario usuario = new Usuario("saras","asd11qwe");
        UsuarioService.guardar(usuario);
    }
    
    @Test
    public void guardar_RegistroNull_registroInvalido() throws IllegalArgumentException, IOException {
        UsuarioService.guardar(null);
    }*/
    
}
