import java.io.IOException;

public class App {
    
    public static void main(String args[]) throws IOException{
        //String aux[] = null;
        if(args.length <= 0){            
            throw new IllegalArgumentException("Se esperan dos argumentos validos para iniciar el programa.");
        }
        else if(args.length == 1){
            throw new IllegalArgumentException("Debe ingresar tanto Usuario como Password.");
        }
        else {
            UsuarioService datos = new UsuarioService();
            Usuario usuario = new Usuario(args[0], args[1]);      
            UsuarioService.guardar(usuario);
        }                  
    }  
}
