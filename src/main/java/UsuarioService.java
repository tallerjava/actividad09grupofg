import java.io.IOException;

public class UsuarioService {
        
    public static void validarPassword(String password) throws IllegalArgumentException{
        
        if(password == null){

            throw new IllegalArgumentException("Argumento invalido. El password debe ser de tipo String, no un valor null");  
            
        }
        
        if(password.equals("")){
            
            throw new IllegalArgumentException("Argumento invalido. El password no puede ser un String vacio");
            
        }
        
        if(password.length() < 6){
            
            throw new IllegalArgumentException("Argumento invalido. El password ingresado debe contener como minimo 8 caracteres");
            
        }
        
        if(password.length() > 20){
            
            throw new IllegalArgumentException("Argumento invalido. El password ingresado debe contener como maximo 20 caracteres");
            
        }
        
        if(!password.matches(".*[0-9].*")){
            
            throw new IllegalArgumentException("Argumento invalido. El password ingresado debe contener al menos un numero");
        
        }
        
        if(password.matches(".*[|].*")){
            
            throw new IllegalArgumentException("Argumento invalido. El password ingresado no debe contener 'pipe'.");
            
        }
          
    }
    
    public static void validarUsername(String username) throws IllegalArgumentException{
        
        if(username == null){

            throw new IllegalArgumentException("Argumento invalido. El username debe ser de tipo String, no un valor null");  
            
        }
        
        if(username.equals("")){
            
            throw new IllegalArgumentException("Argumento invalido. El username no puede ser un String vacio");
            
        }
        
        if(username.length() < 4){
            
            throw new IllegalArgumentException("Argumento invalido. El username ingresado debe contener como minimo 4 caracteres");
            
        }
        
        if(username.length() > 8){
            
            throw new IllegalArgumentException("Argumento invalido. El username ingresado debe contener como maximo 8 caracteres");
            
        }
        
        if(username.matches(".*[|].*")){
            
            throw new IllegalArgumentException("Argumento invalido. El username ingresado no debe contener 'pipe'.");
            
        }
        
    }
    
    public static void guardar(Usuario usuario) throws IOException{
        
        try{
            UsuarioService.validarUsername(usuario.getUsername());
            UsuarioService.validarPassword(usuario.getPassword());
            UsuarioRepository repository = new UsuarioRepository(); 
            repository.guardar(usuario);
            
        }
        
        catch(IOException excepcion){
            System.out.println(excepcion);
        }

    }
    
}