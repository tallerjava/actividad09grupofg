import java.io.*;

public class UsuarioRepository {
    
    public void guardar(Usuario usuario) throws IOException {
        String aux;
            File archivo = new File("Usuarios.txt");
            archivo.createNewFile();
            BufferedReader in = new BufferedReader(new FileReader(archivo));
            BufferedWriter out = new BufferedWriter(new FileWriter(archivo, true)); 
            PrintWriter salida = new PrintWriter(out);
            if(archivo.exists()){
                while((aux = in.readLine()) != null){
                    String[] lista = aux.split("\\|");     
                    if(lista[0].matches(usuario.getUsername())){        
                        throw new IllegalArgumentException("El nombre de usuario ingresado ya se encuentra registrado.");            
                    }
                }
                salida.println(usuario.toString());
                salida.close(); 
            }
    }
}